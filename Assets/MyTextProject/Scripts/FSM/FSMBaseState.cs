﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FSMBaseState
{
    public FSMStateID mStateID { get; set; }

    public FSMBaseState(FSMStateID stateID)
    {
        this.mStateID = stateID;
    }

    public abstract void StateStart();
    public abstract void StateUpdate();
    public abstract void StateEnd();
}
