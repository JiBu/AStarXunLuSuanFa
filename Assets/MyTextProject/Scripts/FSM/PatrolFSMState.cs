﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//敌人巡逻状态
public class PatrolFSMState : FSMBaseState
{
    public PatrolFSMState() : base(FSMStateID.PatrolFSMStateID) { }

    public override void StateStart()
    {
        //状态进入
    }

    public override void StateUpdate()
    {
        //状态更新
    }

    public override void StateEnd()
    {
        //状态结束
    }
}
