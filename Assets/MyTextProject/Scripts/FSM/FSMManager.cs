﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FSMStateID
{
    NullFSMStateID,
    PatrolFSMStateID
}

public class FSMManager
{
    private FSMStateID mCurrentStateID { get; set; }
    private FSMBaseState mCurrentState { get; set; }

    private Dictionary<FSMStateID, FSMBaseState> fsmStateDic = new Dictionary<FSMStateID, FSMBaseState>();


    public void AddFSMState(FSMBaseState state)
    {
        if (state == null)
        {
            Debug.Log("状态值为空");
            return;
        }
        if (fsmStateDic.ContainsKey(state.mStateID))
        {
            Debug.Log("重复包含的状态");
            return;
        }

        fsmStateDic.Add(state.mStateID, state);
    }

    public void DeleteFSMState(FSMBaseState state)
    {
        if (state == null)
        {
            Debug.Log("状态值为空");
            return;
        }
        if (!fsmStateDic.ContainsKey(state.mStateID))
        {
            Debug.Log("容器内没有本状态，无法删除");
            return;
        }

        fsmStateDic.Remove(state.mStateID);
    }

    public void SetFSMState(FSMBaseState state)
    {
        if (state == null)
        {
            Debug.Log("状态为空");
        }
        if (mCurrentState != null)
        {
            mCurrentState.StateEnd();
        }
        this.mCurrentStateID = state.mStateID;
        this.mCurrentState = state;
        mCurrentState.StateStart();
    }

    public void Update()
    {
        mCurrentState.StateUpdate();
    }
}
